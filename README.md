# Test task
You can use Makefile to run seeds and tests. 
`make seed` - for seeding. Seeding command is located in `./backend/src/app/command/Seeder.ts` and compiled as additional file by webpack
`maek test` - for testing

Also, backend has tests and frontend has no cause of time consuming with all setup from scratch

## Necessary:
- Backend service uses express as result it's node server
- Frontend uses nginx server cause of default browser caching and etc. parts of frontend are static and nginx handles more RPS (up to 25k as I can remember), so it's more logical to use it in this case