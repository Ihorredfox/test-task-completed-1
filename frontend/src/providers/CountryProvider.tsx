import * as React from "react";
import {createContext, useReducer} from "react";
import CountryCollection from "../structure/Collections/CountryCollection";
import { CountryStateInterface, CountryReducerActionType, ActionType } from './typings';

export const CountryContext = createContext();

const initialState = {
    countryCollection: new CountryCollection(),
    edit: null,
    delete: null
} as CountryStateInterface;


function reducer(state: CountryStateInterface, action: ActionType) {
    const {countryCollection} = state;

    switch(action.type) {
        case CountryReducerActionType.SET:
            return {...state, countryCollection: action.payload.collection};
        case CountryReducerActionType.ADD:
            countryCollection.add(action.payload.model);
            return {...state, countryCollection: countryCollection.recreate()};
        case CountryReducerActionType.UPDATE:
            countryCollection.update(action.payload.model);
            return {...state, countryCollection: countryCollection.recreate()};
        case CountryReducerActionType.DELETE:
            countryCollection.remove(action.payload.id);
            return {...state, countryCollection: countryCollection.recreate()};
        case CountryReducerActionType.UPDATE_STATE:
            return {...state, edit: action.payload.model}
        case CountryReducerActionType.DELETE_STATE:
            return {...state, delete: action.payload.model}
        case CountryReducerActionType.CREATE_STATE:
            return {...state, create: action.payload.model}
        default: throw new Error(`Undefined action`);
    }
}

interface CountryProviderProps {
    children: React.ReactNode
};

const CountryProvider: React.FC<CountryProviderProps> = ({children}) => {
    const [state, dispatch]: [CountryStateInterface, any] = useReducer(reducer, initialState);

    return (
        <CountryContext.Provider value={[state, dispatch]}>
            {children}
        </CountryContext.Provider>
    )
}

export default CountryProvider;
