import CountryCollection from "../structure/Collections/CountryCollection"
import Country, { CreateCountryRequestData } from "../structure/Models/Country"

export interface CountryStateInterface {
    countryCollection: CountryCollection,
    edit: null | Country,
    delete: null | Country,
    create: null | CreateCountryRequestData
};


export enum CountryReducerActionType {
    LOAD="country/load",
    SET="country/set",
    ADD="country/add",
    DELETE="country/delete",
    UPDATE="country/update",
    DELETE_STATE="country/delete_state",
    UPDATE_STATE="country/update_state",
    CREATE_STATE="country/create_state"
};

export interface ActionInterface {
    type: CountryReducerActionType,
    payload?: any
}

export interface SetActionInterface extends ActionInterface {
    type: CountryReducerActionType.SET,
    payload: {
        collection: CountryCollection
    }
};
export interface AddActionInterface extends ActionInterface {
    type: CountryReducerActionType.ADD,
    payload: {
        model: Country
    }
};
export interface DeleteActionInterface extends ActionInterface {
    type: CountryReducerActionType.DELETE,
    payload: {
        id: string
    }
}
export interface UpdateActionInterface extends ActionInterface {
    type: CountryReducerActionType.UPDATE,
    payload: {
        model: Country
    }
}

export interface DeleteStateInterface extends ActionInterface {
    type: CountryReducerActionType.DELETE_STATE,
    payload: {
        model: Country | null
    };
}
export interface UpdateStateInterface extends ActionInterface {
    type: CountryReducerActionType.UPDATE_STATE,
    payload: {
        model: Country | null
    };
}
export interface CreateStateInterface extends ActionInterface {
    type: CountryReducerActionType.CREATE_STATE,
    payload: {
        model: CreateCountryRequestData | null
    };
}


export type ActionType = SetActionInterface | AddActionInterface | 
    DeleteActionInterface | UpdateActionInterface | DeleteStateInterface | UpdateStateInterface | CreateStateInterface ; 
export type DispatchFunction = (action: ActionInterface) => void;