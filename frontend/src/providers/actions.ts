import CountryCollection from "../structure/Collections/CountryCollection";
import Country, { CreateCountryRequestData } from "../structure/Models/Country";
import { AddActionInterface, CountryReducerActionType, CreateStateInterface, DeleteActionInterface, DeleteStateInterface, SetActionInterface, UpdateActionInterface, UpdateStateInterface } from "./typings";


export const setCollection = (collection: CountryCollection): SetActionInterface => {
    return {
        type: CountryReducerActionType.SET,
        payload: {collection}
    }
}

export const addToCollection = (model: Country): AddActionInterface => {
    return {
        type: CountryReducerActionType.ADD,
        payload: {model}
    }
}

export const deleteFromCollection = (id: string): DeleteActionInterface => {
    return {
        type: CountryReducerActionType.DELETE,
        payload: {id}
    }
}

export const updateInCollection = (model: Country): UpdateActionInterface => {
    return {
        type: CountryReducerActionType.UPDATE,
        payload: {model}
    }
}

export const setDeleteState = (model: Country | null): DeleteStateInterface => {
    return {
        type: CountryReducerActionType.DELETE_STATE,
        payload: {model}
    }
}

export const setUpdateState = (model: Country | null): UpdateStateInterface => {
    return {
        type: CountryReducerActionType.UPDATE_STATE,
        payload: {model}
    }
}

export const setCreateState = (model: CreateCountryRequestData | null): CreateStateInterface => {
    return {
        type: CountryReducerActionType.CREATE_STATE,
        payload: {model}
    }
}