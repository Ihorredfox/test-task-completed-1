import CountryCollection from "../Collections/CountryCollection";
import { backend_url } from '../constants';


export interface CreateCountryRequestData {
    name: string,
    code: string,
    lat: number,
    lng: number,
    flag: string
}

interface CountryAttributesInterface {
    id: string,
    name: string,
    code: string,
    lat: number,
    lng: number,
    flag: string,
    createdAt?: Date,
    updatedAt?: Date
};

// TIP: I made props public to not create get method or a lot of getters & setters
class Country implements CountryAttributesInterface {
    constructor(
        public readonly id: string,
        public name: string,
        public code: string,
        public lat: number,
        public lng: number,
        public flag: string,
        public createdAt?: Date,
        public updatedAt?: Date
    ) {
    }


    recreate(): Country {
        return new Country(this.id, this.name, this.code, this.lat, this.lng, this.flag, this.createdAt, this.updatedAt);
    }

    public static generateDummy(): Country {
        return new Country(
            this.randomString(),
            this.randomString(),
            this.randomString(),
            50.45,
            22.13,
            this.randomString(),
            new Date(),
            new Date()
        )
    }

    private static randomString(): string {
        return (Math.random() + 1).toString(36).substring(2);
    }

    public static async create(data: CreateCountryRequestData): Promise<Country> {
        this.validate(data);
        const response = await fetch(`${backend_url}/country`, {
            method: "POST", 
            body: JSON.stringify(data),
            headers: {
                'Content-type':'application/json'
            }
        });
        const responseData: CountryAttributesInterface = await response.json();
        return new Country(
            responseData.id, 
            responseData.name, 
            responseData.code, 
            responseData.lat, 
            responseData.lng, 
            responseData.flag, 
            responseData.createdAt, 
            responseData.updatedAt
        );
    }

    public async update(): Promise<void> {

        await fetch(`${backend_url}/country/${this.id}`, {
            method: "PUT", 
            body: JSON.stringify(this),
            headers: {
                'Content-type':'application/json'
            }
        });
    }

    public async delete(): Promise<void> {
        await fetch(`${backend_url}/country/${this.id}`, {method: "DELETE"});
    }

    private static validate(data: CreateCountryRequestData): void {
        Object.keys(data).forEach((k) => {
            if(!data[k] && data[k] !== 0) {
                throw new Error(`${k} can't be empty`);
            }
        });
    }

    static async getAll(): Promise<CountryCollection> {
        const response = await fetch(`${backend_url}/country`, {method: "GET"});
        const responseData: {data: CountryAttributesInterface[]} = await response.json();
        return new CountryCollection(responseData.data.map((item: CountryAttributesInterface) => {
            return new Country(item.id, item.name, item.code, item.lat, item.lng, item.flag, item.createdAt, item.updatedAt);
        }));
    }
}

export default Country;