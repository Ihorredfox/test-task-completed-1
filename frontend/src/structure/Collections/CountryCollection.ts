
import Country from '../Models/Country';


class CountryCollection {
    constructor(
        private items: Country[] = []
    ) {}

    get models(): Country[] { return this.items; }
    set models (val: Country[]) { this.items = val; }

    public add(model: Country): void {
        this.items.unshift(model);
    }

    public remove(id: string): void {
        this.items = this.items.filter((item: Country) => {
            return item.id !== id;
        });
    }

    public update(model: Country): void {
        for(let i = 0; i < this.items.length; i++) {
            if(this.items[i].id === model.id) {
                this.items[i] = model;
            }
        }
    }


    public recreate(): CountryCollection {
        return new CountryCollection(this.items);
    }

    public static generateDummy(size: number): CountryCollection {
        const items = [];
        for(let i = 0; i < size; i++){
            items.push(Country.generateDummy());
        }
        return new CountryCollection(items);
    }


    public validate(id: string, name: string): void {
        this.items.forEach((item: Country) => {
            if(item.name === name && item.id !== id) {
                throw new Error("Such name is already exist");
            }
        })
    }
}

export default CountryCollection;