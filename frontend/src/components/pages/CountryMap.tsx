import * as React from "react";
import { CountryStateInterface, DispatchFunction } from '../../providers/typings';
import { CountryContext } from '../../providers/CountryProvider';
import Country from "../../structure/Models/Country";

const createMarker = (map: google.maps.Map, model: Country): google.maps.Marker => {
    console.log("marker created");
    return new google.maps.Marker({
        position: { lat: model.lat, lng: model.lng },
        map,
        icon: {
            url: model.flag,
            scaledSize: new google.maps.Size(10, 10)
        },
    });
}

const createInfoWindow = (model: Country): google.maps.InfoWindow => {
    let content: string = "";
    Object.keys(model).forEach((k) => {
        content += `${k}: ${model[k]} <br>`;
    });
    return new google.maps.InfoWindow({
        content
    });
}


const CountryMap = () => {
    const [state, _]: [CountryStateInterface, DispatchFunction] = React.useContext(CountryContext);
    const [gmap, setGmap]: [google.maps.Map | null, (v: google.maps.Map | null) => void] = React.useState(null);
    const [gmapLoaded, setGmapLoaded] = React.useState(false);


    
    const isGmapLoaded = React.useCallback(() => {
        if(!window.google) {
            setTimeout(() => {
                isGmapLoaded();
            }, 100);
        } else {
            setGmapLoaded(true);
        }
    }, []);

    const initMap = React.useCallback(() => {
        const map = new window.google.maps.Map(
            document.getElementById("map") as HTMLElement,
            {
                zoom: 1,
                center: { lat: -33, lng: 151 },
        });
        setGmap(map);
    }, []);

    React.useEffect(() => {
        if(gmapLoaded) {
            initMap();
        }
    }, [initMap, gmapLoaded]);

    React.useEffect(() => {
        if(gmap && state.countryCollection.models.length > 0) {
            state.countryCollection.models.forEach((model: Country): void => {
                const marker = createMarker(gmap, model);
                const infoWindow = createInfoWindow(model);
                marker.addListener("click", () => {
                    infoWindow.open(gmap, marker);
                });
            });
        }
    }, [gmap, state.countryCollection]);

    React.useEffect(() => {
        isGmapLoaded();
    }, [isGmapLoaded]);

    return (
        <div className="CountryMap">
            <div id="map"></div>
        </div>
    );
}

export default CountryMap;