import * as React from "react";
import CountryTable from "../parts/CountryTable/CountryTable";
import Button from "../utils/Button";
import { CountryStateInterface, DispatchFunction} from '../../providers/typings';
import { CountryContext } from '../../providers/CountryProvider';
import EditPopup from "../parts/popups/EditPopup";
import DeletePopup from "../parts/popups/DeletePopup";
import CreatePopup from "../parts/popups/CreatePopup";
import { setCreateState } from '../../providers/actions';

const CountryPanel = () => {
    const [_, dispatch]: [CountryStateInterface, DispatchFunction] = React.useContext(CountryContext);

    const createCountry = () => {
        dispatch(setCreateState({name: "",code: "",lat: 0,lng: 0,flag: ""}));
    };

    return (
        <div className="CountryPanel">
            <EditPopup />
            <DeletePopup />
            <CreatePopup />
            <CountryTable />
            <Button
                title="Create"
                callback={createCountry.bind(this)}
            >Add country</Button>
        </div>
    );
}

export default CountryPanel;