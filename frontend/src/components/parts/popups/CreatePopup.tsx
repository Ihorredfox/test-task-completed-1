import * as React from "react";
import Popup from "../../utils/Popup";
import Button from "../../utils/Button";
import Country, { CreateCountryRequestData } from '../../../structure/Models/Country';
import { ActionInterface, CountryStateInterface, DispatchFunction} from '../../../providers/typings';
import { CountryContext } from '../../../providers/CountryProvider';
import { addToCollection, setCreateState } from "../../../providers/actions";


const CreatePopup = () => {
    const [state, dispatch]: [CountryStateInterface, DispatchFunction] = React.useContext(CountryContext);
    const [model, setModel]: [CreateCountryRequestData, (v: CreateCountryRequestData) => void] = React.useState(state.edit);
    const [loading, setLoading] = React.useState(false);
    const visible = state.create !== null;

    const submitForm = async (e: Event) => {
        try {
            setLoading(true);
            e.preventDefault();
            const result: Country = await Country.create(model);
            dispatch(addToCollection(result))
        } catch(err: any) {
            alert(err.message);
        }
        setLoading(false);
        close();
    }

    const close = () => {
        dispatch(setCreateState(null));
    }

    const handleChange = (e: Event) => {
        setModel({...model, ...{[e.target.name]: e.target.value}});
    }


    React.useEffect(() => {
        setModel(state.create);
    }, [state.create]);


    if(!model) return (<div></div>)

    return (
        <Popup visible={visible} loader={loading}>
            <form onSubmit={submitForm.bind(this)}>
                <div className="formGroup">
                    <label>Name</label>
                    <input type="text" name={"name"} value={model.name} onChange={handleChange.bind(this)} />
                </div>
                <div className="formGroup">
                    <label>Code</label>
                    <input type="text" name={"code"} value={model.code} onChange={handleChange.bind(this)} />
                </div>
                <div className="formGroup">
                    <label>Flag</label>
                    <input type="text" name={"flag"} value={model.flag} onChange={handleChange.bind(this)} />
                </div>
                <div className="formGroup">
                    <label>Lat</label>
                    <input type="number" name={"lat"} value={model.lat} onChange={handleChange.bind(this)} />
                </div>
                <div className="formGroup">
                    <label>Lng</label>
                    <input type="number" name={"lng"} value={model.lng} onChange={handleChange.bind(this)} />
                </div>
                <div className="buttonGroup">
                    <Button type="submit" title="submit">Create</Button>
                    <Button title="cancel" callback={close.bind(this)}>Cancel</Button>
                </div>
            </form>
        </Popup>
    )
}

export default CreatePopup;