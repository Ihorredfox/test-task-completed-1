import * as React from "react";
import Popup from "../../utils/Popup";
import { CountryStateInterface, DispatchFunction } from '../../../providers/typings';
import { CountryContext } from '../../../providers/CountryProvider';
import Button from "../../utils/Button";
import Country from '../../../structure/Models/Country';
import { setUpdateState, updateInCollection } from "../../../providers/actions";


const EditPopup = () => {
    const [state, dispatch]: [CountryStateInterface, DispatchFunction] = React.useContext(CountryContext);
    const [model, setModel]: [Country, (v: Country) => void] = React.useState(state.edit);
    const [loading, setLoading] = React.useState(false);
    const visible = state.edit !== null;

    const submitForm = async (e: Event) => {
        try {
            setLoading(true);
            e.preventDefault();
            state.countryCollection.validate(model.id, model.name);
            await model.update();
            dispatch(updateInCollection(model));
        } catch(err: any) {
            alert(err.message);
        }
        setLoading(false);
        close();
    }

    const close = () => {
        dispatch(setUpdateState(null));
    }

    const handleChange = (e: Event) => {
        model[e.target.name] = e.target.value;
        setModel(model.recreate());
    }


    React.useEffect(() => {
        if(state.edit instanceof Country) {
            setModel(state.edit.recreate());
        }
    }, [state.edit]);


    if(!model) return (<div></div>)

    return (
        <Popup visible={visible} loader={loading}>
            <form onSubmit={submitForm.bind(this)}>
                <div className="formGroup">
                    <label>Name</label>
                    <input type="text" name={"name"} value={model.name} onChange={handleChange.bind(this)} />
                </div>
                <div className="formGroup">
                    <label>Code</label>
                    <input type="text" name={"code"} value={model.code} onChange={handleChange.bind(this)} />
                </div>
                <div className="formGroup">
                    <label>Flag</label>
                    <input type="text" name={"flag"} value={model.flag} onChange={handleChange.bind(this)} />
                </div>
                <div className="formGroup">
                    <label>Lat</label>
                    <input type="number" name={"lat"} value={model.lat} onChange={handleChange.bind(this)} />
                </div>
                <div className="formGroup">
                    <label>Lng</label>
                    <input type="number" name={"lng"} value={model.lng} onChange={handleChange.bind(this)} />
                </div>
                <div className="buttonGroup">
                    <Button type="submit" title="submit">Update</Button>
                    <Button title="cancel" callback={close.bind(this)}>Cancel</Button>
                </div>
            </form>
        </Popup>
    )
}

export default EditPopup;