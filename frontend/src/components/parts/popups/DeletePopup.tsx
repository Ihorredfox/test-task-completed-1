import * as React from "react";
import Popup from "../../utils/Popup";
import { CountryStateInterface, DispatchFunction } from '../../../providers/typings';
import { CountryContext } from '../../../providers/CountryProvider';
import Button from "../../utils/Button";
import { setDeleteState, deleteFromCollection } from '../../../providers/actions';


const DeletePopup = () => {
    const [state, dispatch]: [CountryStateInterface, DispatchFunction] = React.useContext(CountryContext);
    const [loading, setLoading] = React.useState(false);
    const visible = state.delete !== null;
    const model = state.delete;

    const submitDelete = async () => {
        setLoading(true);
        try {
            const temp = model.id;
            await model.delete();
            dispatch(deleteFromCollection(temp));
        } catch(e: any) {
            alert(e.message);
        }
        setLoading(false);
        close();

    }

    const close = () => {
        dispatch(setDeleteState(null));
    }

    if(!model) return (<div></div>)

    return (
        <Popup visible={visible} loader={loading}>
            <p>Are you sure you want to delete {model.name} ?</p>
            <div className="buttonGroup">
                <Button type="submit" title="submit" callback={submitDelete.bind(this)}>Delete</Button>
                <Button title="cancel" callback={close.bind(this)}>Cancel</Button>
            </div>
        </Popup>
    )
}

export default DeletePopup;