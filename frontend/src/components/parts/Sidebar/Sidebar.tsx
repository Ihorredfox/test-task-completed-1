import * as React from "react";
import { Link } from "react-router-dom";
import Button from "../../utils/Button";

export const Sidebar = () => {
    
    return (
        <div className="Sidebar">
            <Button
                title="Panel"
            >
                <Link to="/" style={{width:"100%"}}>Panel</Link>   
            </Button>
            <Button
                title="Map"
            >
                <Link to="/map" style={{width:"100%"}}>Map</Link>
            </Button>
        </div>
    );
}