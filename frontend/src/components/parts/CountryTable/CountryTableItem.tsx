import * as React from "react";
import Country from "../../../structure/Models/Country";
import Button from "../../utils/Button";
import { CountryContext } from '../../../providers/CountryProvider';
import { CountryStateInterface, DispatchFunction } from '../../../providers/typings';
import { setUpdateState, setDeleteState } from "../../../providers/actions";

interface CountryTableItemProps {
    model: Country,
    key: string
};

const CountryTableItem = ({model}: CountryTableItemProps) => {
    const [_, dispatch]: [CountryStateInterface, DispatchFunction] = React.useContext(CountryContext);

    return (
        <tr>
            <td>{model.name}</td>
            <td>{model.code}</td>
            <td>{model.flag}</td>
            <td>
                <Button title="update" callback={() => dispatch(setUpdateState(model))}>Update</Button>
                <Button title="delete" callback={() => dispatch(setDeleteState(model))}>Delete</Button>
            </td>
        </tr>
    );
}

export default CountryTableItem;