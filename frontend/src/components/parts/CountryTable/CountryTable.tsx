import * as React from "react";
import { CountryContext } from '../../../providers/CountryProvider';
import { CountryStateInterface, DispatchFunction } from '../../../providers/typings';
import Country from "../../../structure/Models/Country";
import CountryTableItem from "./CountryTableItem";


const CountryTable = () => {
    const [state, _]: [CountryStateInterface, DispatchFunction] = React.useContext(CountryContext);

    return (
        <div className="CountryTable">
            <table cellSpacing="2" border="2" cellPadding="5">
                <thead>
                    <tr>
                        <td>Name</td>
                        <td>Code</td>
                        <td>Flag</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    {
                        state.countryCollection.models.map((model: Country) => <CountryTableItem key={model.id}  model={model} />)
                    }
                </tbody>
            </table>
        </div>
    );
}

export default CountryTable;