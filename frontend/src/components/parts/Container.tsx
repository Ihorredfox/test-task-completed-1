import * as React from "react";
import Country from "../../structure/Models/Country";
import { Sidebar } from "./Sidebar/Sidebar";
import { setCollection } from '../../providers/actions';
import { CountryStateInterface, DispatchFunction } from '../../providers/typings';
import { CountryContext } from '../../providers/CountryProvider';

const Container: React.FC<{children: React.ReactNode}> = ({children}: {children: React.ReactNode}) => {
    const [_, dispatch]: [CountryStateInterface, DispatchFunction] = React.useContext(CountryContext);
    
    React.useEffect(() => {
        (async function(): Promise<void>{
            const collection = await Country.getAll();
            dispatch(setCollection(collection));
        })();
    }, [dispatch]);
    
    
    return (
        <div className="Container">
            <Sidebar />
            <div className="sideComponent">
                {children}
            </div>
        </div>
    )
}

export default Container;

