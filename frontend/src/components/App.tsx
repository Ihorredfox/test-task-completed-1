import * as React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import '../assets/styles/App.css';
import Container from "./parts/Container";
import CountryMap from "./pages/CountryMap";
import CountryPanel from './pages/CountryPanel';
import CountryProvider from "../providers/CountryProvider";


const App = () => {
  return (
    <div className="App">
      <Router>
        <CountryProvider>
          <Container>
            <Routes>
                <Route path="/" element={<CountryPanel />} />
                <Route path="/map" element={<CountryMap />} />
            </Routes>
          </Container>
        </CountryProvider>
      </Router>
    </div>
  );
}

export default App;
