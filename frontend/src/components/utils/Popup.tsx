import * as React from "react";

interface PopupProps {
    children: React.ReactNode,
    visible: boolean,
    loader?: boolean
}

const Popup: React.FC<PopupProps> = ({children, visible, loader = false}: PopupProps) => {


    return (
        <div className="Popup" style={{display: visible ? "flex": "none"}}>
            <div className="Popup-inner">
                {loader ? <div className="Popup-loader">Loading...</div> : ""}
                {visible?children:""}
            </div>
        </div>
    );
}

export default Popup;