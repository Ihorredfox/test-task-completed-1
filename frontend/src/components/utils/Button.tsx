import * as React from "react";

interface ButtonProps {
    children: React.ReactNode,
    callback?: (e: Event) => void,
    title: string,
    className?: string,
    type?: string
}


const Button: React.FC<ButtonProps> = (props: ButtonProps) => {
    return (
        <button
            className={props.className ? `btn ${props.className}` : "btn"}
            onClick={props.callback ? props.callback.bind(this) : null}
            title={props.title}
            type={!props.type? "button": props.type}
        >
            {props.children}
        </button>
    )
};
export default Button;