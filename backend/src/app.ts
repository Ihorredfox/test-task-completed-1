import express, {Request, Response, NextFunction} from "express";
import country from "./routes/country";
import Cors from './app/middleware/Cors';

const version1 = "/api/v1";
const app = express();
app.use(express.json());
app.use(Cors.handle);
app.use(`${version1}/country`, country);

export default app;