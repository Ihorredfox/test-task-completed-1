import { body, oneOf, param, query } from 'express-validator';


export default class CountryValidator {
    public validateCreate() {
        return [
            body('name').exists().notEmpty().isString().withMessage('The name field should not be empty'),
            body('code').exists().notEmpty().isString().withMessage('The code field should not be empty'),
            body('lat').exists().notEmpty().isNumeric().withMessage('The lat field should not be empty'),
            body('lng').exists().notEmpty().isNumeric().withMessage('The lng field should not be empty'),
            body('flag').exists().notEmpty().isString().withMessage('The flag field should not be empty')
        ];
    }

    public validateIdParam() {
        return [
            param('id').notEmpty().isString().withMessage('Invalid id')
        ]
    }

    public validateUpdate() {
        return [
            oneOf([
                body('name').exists().notEmpty().isString().withMessage('The name field should not be empty'),
                body('code').exists().notEmpty().isString().withMessage('The code field should not be empty'),
                body('lat').exists().notEmpty().isNumeric().withMessage('The lat field should not be empty'),
                body('lng').exists().notEmpty().isNumeric().withMessage('The lng field should not be empty'),
                body('flag').exists().notEmpty().isString().withMessage('The flag field should not be empty')
            ])
        ]
    }
}